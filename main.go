package main

import (
	"log"
	"net/http"
	"os/exec"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func main() {
	log.Printf("Started API server, listening on port 9000..")
	router := chi.NewRouter()
	router.Use(middleware.Logger, middleware.Recoverer)
	router.Get("/libreelec", libreelec)
	http.ListenAndServe(":9000", router)
}

func libreelec(w http.ResponseWriter, r *http.Request) {
	cmd := exec.Command("ping", "-c", "2", "192.168.178.39")
	_, err := cmd.Output()
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("FAILED"))
	} else if err == nil {
		w.Write([]byte("OK"))
	}
}
